from sqlalchemy import Column, VARCHAR, String, Boolean, Integer

from database import Base


class Account(Base):
    __tablename__ = 'accounts'

    id = Column(VARCHAR(64), primary_key=True)
    # username = Column(String, unique=True)
    # password = Column(String)
    api_key = Column(String)
    api_secret = Column(String)
    referral_percent = Column(Integer, default=0)
    status = Column(Boolean, default=True)

    def __str__(self):
        return str(self.id)
