from sqlalchemy import Column, Integer, Float, VARCHAR, DateTime, Date, ForeignKey, Boolean

from datetime import datetime
from database import Base


class Trade(Base):
    __tablename__ = 'trades'

    id = Column(VARCHAR(64), primary_key=True)
    account_id = Column(VARCHAR(64), ForeignKey("accounts.id"), nullable=False)

    commission = Column(Float)
    referral_bonus = Column(Float)
    trades_count = Column(Integer)

    date = Column(Date, default=datetime.now().date)
    paid = Column(Boolean, default=False)
    created_at = Column(DateTime, default=datetime.now().utcnow)

    def __str__(self):
        return f"Account <{self.account_id}> {self.date}: {self.commission}"
