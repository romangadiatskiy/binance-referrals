import os
import sys

from sqlalchemy.orm import sessionmaker
from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base

from settings import logging


sys.path.append(os.path.dirname(os.path.realpath(__file__)))


default_db_url = 'postgresql://binance_user:3K43wWsDLjf2"Ns7@46.101.125.2:5432/binance_commissions_monitoring'
db_url = os.environ.get('DATABASE_URL', default_db_url)

logging.debug(f"Creating session with the {db_url}")
engine = create_engine(db_url)
Session = sessionmaker(bind=engine)
logging.debug(f"Session with the {db_url} is created")


Base = declarative_base()


def create_db():
    Base.metadata.create_all(engine)
    logging.debug("DB has been created")
