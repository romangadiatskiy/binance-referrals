import hmac
import hashlib

import calendar
import time

from datetime import datetime, timedelta

import pandas as pd

from binance import Client, exceptions

from settings import API_KEY, API_SECRET
from settings import logging
from settings import REQUEST_ATTEMPTS

from database.models.accounts import Account
from database.models.trades import Trade
from database import Session, create_db

from multiprocessing.dummy import Pool


class BinanceCommissionParser:
    def __init__(self, account_id, api_key, api_secret):
        self.account_id = account_id

        self.__API_KEY = api_key
        self.__API_SECRET = api_secret

        self.client = Client(api_key=self.__API_KEY,
                             api_secret=self.__API_SECRET)

    @staticmethod
    def get_unix_ms_from_date(date):
        return int(calendar.timegm(date.timetuple()) * 1000 + date.microsecond/1000)

    def hashing(self, query_string):
        return hmac.new(self.__API_SECRET.encode('utf-8'), query_string.encode('utf-8'), hashlib.sha256).hexdigest()

    def get_futures_symbols(self):
        return [price.get('symbol') for price in self.client.futures_ticker()]

    def get_first_trade_id_from_start_date(self, symbol, from_date, to_date):
        response = self.client.futures_account_trades(
            symbol=symbol,
            startTime=self.get_unix_ms_from_date(from_date),
            endTime=self.get_unix_ms_from_date(to_date)
        )

        if len(response) > 0:
            return response[0].get('id') - 1
        else:
            return None

    def trim(self, df, to_date):
        return df[df['time'] <= self.get_unix_ms_from_date(to_date)]

    def _parse_symbol(self, symbol, date, limit=1000):
        df = pd.DataFrame()
        current_time = 0
        request_errors = 0

        start_time = date.replace(hour=0, minute=0, second=0, microsecond=0)
        end_time = date.replace(hour=23, minute=59, second=59, microsecond=999999)

        from_id = self.get_first_trade_id_from_start_date(symbol=symbol,
                                                          from_date=start_time,
                                                          to_date=end_time)

        while current_time < self.get_unix_ms_from_date(end_time) and request_errors < REQUEST_ATTEMPTS:
            try:
                trades = self.client.futures_account_trades(
                    symbol=symbol,
                    fromId=from_id,
                    limit=limit
                )

            except (Exception, exceptions.BinanceAPIException) as e:
                request_errors += 1
                logging.warning(f'Got exception [account_id = {self.account_id}]: {e}')
                time.sleep(3)
            else:
                current_time = trades[-1].get('time') if trades else 0
                from_id = trades[-1].get('id') if trades else None

                df = pd.concat([df, pd.DataFrame(trades)])
                if not from_id or not trades or len(trades) < limit:
                    break

                # don't exceed request limits
                time.sleep(0.5)

        if not df.empty:
            df.drop_duplicates(subset='id', inplace=True)
            df = self.trim(df, end_time)

            df[['commission']] = df[['commission']].astype(float)
            commission = df['commission'].sum()
        else:
            commission = 0

        return {'trades_count': len(df),
                'commission': commission}

    def run(self, date=None):
        if not date:
            date = datetime.now()

        trade = {
            'trades_count': 0,
            'commission': 0,
            'date': date
        }

        symbols = self.get_futures_symbols()
        symbols_count = len(symbols)

        for i, symbol in enumerate(symbols, start=1):
            symbol_trade = self._parse_symbol(symbol=symbol, date=date)

            logging.info(f"{i} / {symbols_count} [account_id = {self.account_id}] [{symbol}]: "
                         f"commission = {symbol_trade.get('commission')}; "
                         f"count = {symbol_trade.get('trades_count')}")

            trade['trades_count'] += symbol_trade.get('trades_count')
            trade['commission'] += symbol_trade.get('commission')

        logging.info(f"Total [account_id = {self.account_id}]: {trade}")
        return trade


def parse(account: Account):
    date = (datetime.now() - timedelta(days=1))
    # date = datetime.now()

    try:
        parser = BinanceCommissionParser(account_id=account.id,
                                         api_key=account.api_key,
                                         api_secret=account.api_secret)

        trade = parser.run(date=date)

        try:
            referral_bonus = trade.get('commission', 0) / 100 * account.referral_percent
        except (Exception,):
            referral_bonus = 0

        trade.update({
            'id': parser.hashing(account.id + str(date.date())),
            'account_id': account.id,
            'referral_bonus': referral_bonus
        })

    except(Exception,) as e:
        logging.error(f'Got parsing exception:: {e}')
    else:
        session = Session()

        try:
            session.merge(Trade(**trade))
            session.commit()
        except (Exception,):
            session.rollback()
        finally:
            session.close()


class BinanceCommissionBot:
    def __init__(self):
        self.session = Session()
        create_db()

        logging.debug(f"Initial:: {dict(self.__dict__)}")

    def __del__(self):
        if self.session:
            try:
                self.session.close()
            except (Exception,):
                pass

    def add_account(self, api_key=API_KEY, api_secret=API_SECRET):
        account_id = hmac.new(api_secret.encode('utf-8'), api_key.encode('utf-8'), hashlib.sha256).hexdigest()

        try:
            self.session.merge(Account(id=account_id, api_key=api_key, api_secret=api_secret))
            self.session.commit()
        except (Exception,) as e:
            logging.warning(f"Cant add the account [{api_key}:{api_secret}]:: {e}")
            self.session.rollback()
        else:
            logging.info(f"{account_id} account has been added to the database")

    def get_accounts(self):
        return self.session.query(Account).all()

    def run(self, processes=1):
        start = datetime.now()
        accounts = self.get_accounts()

        with Pool(processes=processes) as pool:
            pool.map(parse, accounts)

        logging.info(f"Execution time: {datetime.now() - start}")


if __name__ == "__main__":
    BinanceCommissionBot().run(processes=5)
